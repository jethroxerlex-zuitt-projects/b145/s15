// include a confirm message
// console.log() is used to output/display messages in the console of your browser.
console.log("Hello from JS");

// there are multiple ways to display messages in the console using different formats.

// error message
console.error('Error something');

// warning message
console.warn("Ooops Warning")

// syntax:
// function nameofFunction () {
// 	instruction/procedures
// 	return
// }

// lets create a function that will display an error
function errorMessage(){
	console.error("Error oh no!!!!");
}
errorMessage(); //callout the function
function greetings() { //DECLARATION
	//procedures 
	console.log('Salutations from JS!');
}


// INVOCATION
greetings();
// There are different methods in declaring a function in JS

// [SECTION 1] Variables declared outside a function can be used INSIDE a function.

let givenName = 'John';
let familyName = 'Smith';

// Create a function that will utilize the information outside it's scope.
function fullName() {
	//combine the values of the information outside and display it inside the console.
	console.log(givenName + ' ' + familyName);
}
//Invoking or Calling out functions we do it, by adding parenthesis() after the function name.
fullName();

//[SECTION 2] "blocked scope" variables, it is a variable that can only be used within the scope of the function.

//Create a function that will allow us to compute the values of 2 variables.
function computeTotal() {
	let numA = 20;
	let numB = 5;
	// add the values and display it inside the console.
	console.log(numA + numB);
} 

//console.log(numA); the variable cannot be used outside the scope of the function where it was declared
//callout the function
computeTotal();

//[SECTION 3] Functions with Parameters

//Parameter - acts as a variable or a container/catchers that only exists inside a function. A parameter is used to store information that is provided to a function when it is called or invoked.

// Create a function that will emulate a pokemon battle.

function pokemon (pangalan) {
  //were going to use the "parameter" declared on this function to be processed and displayed inside the console.
  console.log('I Choose you:' + pangalan); 
}

//invocation
pokemon('Pikachu');

//parameters vs arguments?

//Argument - is the "ACTUAL" value that is provided inside a function for it to work properly. the term argument is used when functions are called/invoke as compared to parameters when a function is simply declared.

//[SECTION 4] Functions with multiple parameters

// Declare a function that will get the sum of multiple values.

function addNumbers(numA, numB, numC, numD, numE) {
	//display the sum inside the console
  console.log (numA + numB + numC + numD + numE);
}
//NaN: Not a Number
addNumbers(1,2,3,4,5); 

//Create a function that will generate a person's full Name
function createFullName(fName,mName,lName){
  console.log (lName +',' + fName + ' '+ mName)  
}

//invoke the function and pass down the arguments needed that will take the place of each parameter.
createFullName('Juan', 'Dela', 'Cruz');
// upon using multiple arguments it will correspond to the number of "parameters" declared in a function in succeeding order. unless  reordered inside the function

//[SECTION 5] Using variables as Arguments

// Create a function that will display the stats of a pokemon in battle. 

let attackA = 'Tackle';
let attackB = 'Thunderbolt';
let attackC =  'Quick Attack';
let selectedPokemon = 'Ratata';

function pokemonStats(name, attack){
	console.log(name + ' use ' + attack);
}

pokemonStats(selectedPokemon, attackC);

//Note: using variables as arguments will allow us to utilize code reusability, this will help us reduce the amount of code that we have to write down.

// The use of the "return" expression/statement
// the return statement is used to display the output of a function.

//[SECTION 6] the RETURN statement

// the "return" statement allows the output of a function to be passed to the line/block of code that called the function

//create a function that will return a string message.
function returnString() {
	return 'Hello World'
	       2 + 1;
}

// lets repackage the result of this function inside a new variable
//returnString();
let functionOutput = returnString();
console.log(functionOutput);

// Create a simple function to demonstrate the use and behavior of the return statement again.

function dialog () {
	console.log('Oooops! I did it Again')
	console.log("Dont you know that you're toxic");
	return 'Im a Slave for you!'
	console.log('Sometimes I run!') 
}
// note: any Block/line of code that will come after our "return" statement is going to be ignored because it came after the end of the function execution. The main purpose of the "return" statement is to identify which will be the final output/result of the function at the same time determine the end of the function statement.

// invocation
console.log(dialog());

// [SECTION 7] Using Functions as Arguments
// Function parameters can also accept other function as their arguments.
// Some complex functions use other function as their arguments to perform more complicated/complex results.

// create a simple function to be used as an argument later
function argumentSaFunction (){
	console.log('This function was passed as an argument');
}

function invokeFunction (argumentNaFunction, pangalawangFunction){
  argumentNaFunction();
  pangalawangFunction( selectedPokemon, attackB);
  pangalawangFunction(selectedPokemon,attackA)
}

// invocation
invokeFunction(argumentSaFunction, pokemonStats);

// IF YOU WANT to see details/information about a function
console.log(invokeFunction);